var fishing = require("./scripts/fishing/fishing");
var mining = require("./scripts/mining/mining");
var robot = require("robotjs");
var tools = require("./scripts/global/tools");

function main(script) {
  switch (script) {
    case "FISHING":
      //fishing.position();
      fishing.fishing();
      break;
    case "MINING":
      mining.mining();
      break;
  }
}

main("MINING");
// while (1) {
//   console.log(robot.getMousePos());
// }

// const colors = new Set();
// while (1) {
//   robot.moveMouseSmooth(1517, 350);
//   colors.add(robot.getPixelColor(1384, 356));
//   console.log(colors);
// }
