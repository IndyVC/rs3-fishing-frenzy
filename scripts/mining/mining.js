const robot = require("robotjs");
const tools = require("../global/tools");
const c = require("./constants");

function mining() {
  while (1) {
    const box = tools.randomBox(c.point, 10);
    robot.moveMouseSmooth(box.x, box.y, Math.random() * 2 + 0.2);
    tools.sleep(600 * Math.random());
    robot.mouseClick();
    // if ([51, 52, 53, 54].includes(new Date().getSeconds())) {
    //   console.log("reached");
    //   const box = tools.randomBox(c.miningSkilTab, 3);
    //   robot.moveMouseSmooth(box.x, box.y, Math.random() * 2);
    //   tools.sleep(600 * (Math.random() * 2 + 0.2));
    //   tools.press();
    // }
    tools.sleep(180000 * (Math.random() + 0.2));
  }
}
module.exports = {
  mining,
};
