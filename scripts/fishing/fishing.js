const robot = require("robotjs");
const tools = require("../global/tools");
const c = require("./constants");
const global = require("../global/functions");
const { sleep } = require("../global/tools");

function position() {
  robot.moveMouseSmooth(tools.mp().x, tools.mp().y);
  robot.mouseClick();
  for (let i = 0; i < 21; i++) {
    robot.keyTap("right");
  }
  for (let i = 0; i < 5; i++) {
    robot.keyTap("down");
  }
}

function fishing() {
  let missing = 0;
  for (let i = 0; i < c.fishing_spots.length; i++) {
    if (missing > 300) {
      if (alterPosition(missing)) {
        missing = 0;
      }
    }
    tools.sleep(10);
    //fetch spot
    const spot = c.fishing_spots[i];
    const box = tools.randomBox(spot, 2);
    robot.moveMouse(box.x, box.y); //, Math.random() * 2 + 0.2

    //check if spot is present
    const sample = robot.getPixelColor(box.x, box.y);
    if (
      c.fishing_colors.includes(sample) ||
      (sample[0] == "d" &&
        (sample[1] >= 3 || ["a", "b", "c", "d", "e", "f"].includes(sample[1])))
    ) {
      robot.mouseClick();
      console.log(sample);
      missing = 0;
      sleep(3000);
      let current = robot.getPixelColor(
        robot.getMousePos().x,
        robot.getMousePos().y
      );
      //Stop looking until spot is finished fishing
      let tolerant = 0;
      while (1) {
        current = robot.getPixelColor(
          robot.getMousePos().x,
          robot.getMousePos().y
        );
        if (
          c.fishing_colors.includes(current) ||
          (current[0] == "d" &&
            (current[1] >= 3 ||
              ["a", "b", "c", "d", "e", "f"].includes(current[1])))
        ) {
        } else {
          tolerant = tolerant + 1;
        }
        //Spot is finished if tolerant is exceeded
        if (tolerant >= 100) {
          break;
        }
      }
      //global.showTime("FISHING");
    } else {
      missing++;
    }

    console.log(`index: ${i} -- missing: ${missing}`);
    if (i === c.fishing_spots.length - 1) {
      i = -1;
    }
  }
}

function alterPosition(missing) {
  let worked = false;
  const color = robot.getPixelColor(tools.mp().x - 63, tools.mp().y);
  robot.moveMouseSmooth(tools.mp().x - 63, tools.mp().y);
  if (c.colors_left.includes(color)) {
    robot.moveMouseSmooth(c.reposition_left.x, c.reposition_left.y);
    robot.mouseClick();
    console.log("left mispositioned");
    worked = true;
  } else if (c.colors_right.includes(color) || missing >= 330) {
    robot.moveMouseSmooth(c.reposition_right.x, c.reposition_right.y);
    robot.mouseClick();
    console.log("right mispositioned");
    worked = true;
  } else {
    console.log("cant detect", color);
    worked = false;
  }
  sleep(3000);
  return worked;
}

module.exports = {
  fishing,
  position,
};
