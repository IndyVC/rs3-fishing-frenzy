var Jimp = require("jimp");
var robot = require("robotjs");
var tools = require("./tools");
const { randomBox, sleep } = require("./tools");
var width = robot.getScreenSize().width,
  height = robot.getScreenSize().height;
var path = "myfile.png";
var rimg;

const skill_tab = { x: 1866, y: 262 };
const skill_fishing = { x: 1790, y: 115 };

function showSkill(skill) {
  const box = tools.randomBox(skill_tab, 2);
  robot.moveMouseSmooth(box.x, box.y);
  robot.mouseClick();
  tools.sleep(500);
  switch (skill) {
    case "FISHING":
      robot.moveMouseSmooth(skill_fishing.x, skill_fishing.y);
      tools.press();
      sleep(800);
      break;
  }
  robot.moveMouseSmooth(box.x, box.y);
  robot.mouseClick();
  sleep(200);
  robot.mouseClick();
}

function showTime(show) {
  let randomNr = tools.getRandomInt(1, 500);
  if (randomNr == 250) {
    showSkill(show);
  }
}

module.exports = {
  showSkill,
  showTime,
};
