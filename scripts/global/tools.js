var Jimp = require("jimp");
var robot = require("robotjs");
var width = robot.getScreenSize().width,
  height = robot.getScreenSize().height;
var path = "myfile.png";
var rimg;

function randomBox(point, margin) {
  margin = margin * Math.random();
  const boxes = [
    { x: point.x - margin, y: point.y - margin },
    { x: point.x, y: point.y - margin },
    { x: point.x + margin, y: point.y - margin },
    { x: point.x - margin, y: point.y },
    { x: point.x, y: point.y },
    { x: point.x + margin, y: point.y },
    { x: point.x - margin, y: point.y + margin },
    { x: point.x, y: point.y + margin },
    { x: point.x + margin, y: point.y + margin },
  ];

  const index = Math.floor(Math.random() * boxes.length);

  return boxes[index];
}

function sleep(ms) {
  let start = new Date().getTime(),
    expire = start + ms;
  while (new Date().getTime() < expire) {}
  return;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function captureImage() {
  var jimg = new Jimp(width, height);
  rimg = robot.screen.capture(width / 2, 0, width / 2, height / 2);

  for (var x = 0; x < width; x++) {
    for (var y = 0; y < height; y++) {
      var index = y * rimg.byteWidth + x * rimg.bytesPerPixel;
      var r = rimg.image[index];
      var g = rimg.image[index + 1];
      var b = rimg.image[index + 2];
      var num = r * 256 + g * 256 * 256 + b * 256 * 256 * 256 + 255;
      jimg.setPixelColor(num, x, y);
    }
  }
  jimg.write(path);
}

function findPosition(colors) {
  let x = width / 2,
    y = 0;
  for (var i = 0; i < 20000; i++) {
    var random_x = getRandomInt(0, width / 2 - 1);
    var random_y = getRandomInt(0, height / 2 - 1);
    var sample_color = rimg.colorAt(random_x, random_y);

    if (colors.includes(sample_color)) {
      // because we took a cropped screenshot, and we want to return the pixel position
      // on the entire screen, we can account for that by adding the relative crop x and y
      // to the pixel position found in the screenshot;

      var screen_x = random_x + x;
      var screen_y = random_y + y;
      console.log(sample_color);
      return { x: screen_x, y: screen_y };
    }
  }
}

function press() {
  robot.mouseToggle("down");
  sleep(1000);
  robot.mouseToggle("up");
}

function mp() {
  return { x: (width / 4) * 3, y: height / 4 };
}
module.exports = {
  randomBox,
  findPosition,
  captureImage,
  sleep,
  getRandomInt,
  press,
  mp,
};
